This repository contains solutions to the exercises found in "The C Programming
Language, Second Edition" by Brian W. Kernighan and Dennis M. Ritchie, sometimes
referred to as simply "K&R".

The exercises and their solutions are based off of the 47th printing of this
book in February 2011, ISBN 0-13-110370-9.
