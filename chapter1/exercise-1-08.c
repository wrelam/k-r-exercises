/*******************************************************************************
    Exercise 1-8

    Write a program to count blanks, tabs, and newlines.
*******************************************************************************/
#include <stdio.h>

main()
{
    int c, blanks, tabs, newlines;

    blanks = 0;
    tabs = 0;
    newlines = 0;

    while ((c = getchar()) != EOF)
    {
        if (c == ' ')
            blanks++;
        else if (c == '\t')
            tabs++;
        else if (c == '\n')
            newlines++;
    }

    printf("%d Blanks\n", blanks); 
    printf("%d Tabs\n", tabs); 
    printf("%d Newlines\n", newlines); 
}
