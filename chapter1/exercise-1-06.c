/*******************************************************************************
    Exercise 1-6

    Verify that the expression getchar() != EOF is 0 or 1.
*******************************************************************************/
#include <stdio.h>

main()
{
    int c;

    while (c = (getchar() != EOF))
    {
        if (1 != c)
        {
            printf("Result: %d\n", c);
            break;
        }
    }

    /* Will always print 0 */
    printf("Result: %d\n", c); 
}
