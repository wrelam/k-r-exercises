/*******************************************************************************
    Exercise 1-13

    Write a program to print a histogram of the lengths of words in its input.
    It is easy to draw the histogram with the bars horizontal; a vertical
    orientation is more challenging.
*******************************************************************************/
#include <stdio.h>

/* A word is any sequence of characters that does not contain a blank, tab, or
 * newline. Only handles words up to 80 characters in length with a maximum
 * reported count of 78 characters..
 */
#define IN  1    /* inside a word */
#define OUT 0    /* outside a word */

main()
{
    int i, j, c, state, counts[81];

    state = OUT;

    for (i = 0; i < sizeof(counts)/sizeof(counts[0]); ++i)
        counts[i] = 0;

    i = 0;
    while ((c = getchar()) != EOF) {
        if (c == ' ' || c == '\n' || c == '\t') {
            state = OUT;
            if (i < sizeof(counts))
                counts[i]++;
            i = 0;
        }
        else {
            if (state == OUT) {
                state = IN;
            }
            ++i;
        }
    }

    printf("Word length histogram\n");
    for (i = 1; i < sizeof(counts)/sizeof(counts[0]); ++i) {
        printf("%d: ", i);
        if (i < 10)
            printf(" ");
        for (j = 0; j < counts[i]; ++j) {
            printf("#");
        }
        printf("\n");
    }
}
